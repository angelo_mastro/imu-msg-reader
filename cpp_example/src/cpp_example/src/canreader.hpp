#ifndef H_CANREADER
#define H_CANREADER

#include <string>

namespace canreader
{


class Reader
{
public:

    Reader(std::string const& filepath);

    bool next(std::string& line);

    void read(unsigned id);

private:
    std::string mFilepath;

};


}


#endif
