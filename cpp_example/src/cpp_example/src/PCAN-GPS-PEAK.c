#include "PCAN-GPS-PEAK.h"
#include <inttypes.h>

#define UNUSED(X) ((void)(X))

static inline uint64_t reverse_byte_order(uint64_t x)
{
	x = (x & 0x00000000FFFFFFFF) << 32 | (x & 0xFFFFFFFF00000000) >> 32;
	x = (x & 0x0000FFFF0000FFFF) << 16 | (x & 0xFFFF0000FFFF0000) >> 16;
	x = (x & 0x00FF00FF00FF00FF) << 8  | (x & 0xFF00FF00FF00FF00) >> 8;
	return x;
}


int unpack_message(unsigned id, uint64_t data, uint8_t dlc)
{
	switch(id) {
	case 0x600: return unpack_can_0x600_BMC_Acceleration(&can_0x600_BMC_Acceleration_data, data, dlc);
	case 0x601: return unpack_can_0x601_BMC_MagneticField(&can_0x601_BMC_MagneticField_data, data, dlc);
	case 0x610: return unpack_can_0x610_L3GD20_Rotation_A(&can_0x610_L3GD20_Rotation_A_data, data, dlc);
	case 0x611: return unpack_can_0x611_L3GD20_Rotation_B(&can_0x611_L3GD20_Rotation_B_data, data, dlc);
	case 0x620: return unpack_can_0x620_GPS_Status(&can_0x620_GPS_Status_data, data, dlc);
	case 0x621: return unpack_can_0x621_GPS_CourseSpeed(&can_0x621_GPS_CourseSpeed_data, data, dlc);
	case 0x622: return unpack_can_0x622_GPS_PositionLongitude(&can_0x622_GPS_PositionLongitude_data, data, dlc);
	case 0x623: return unpack_can_0x623_GPS_PositionLatitude(&can_0x623_GPS_PositionLatitude_data, data, dlc);
	case 0x624: return unpack_can_0x624_GPS_PositionAltitude(&can_0x624_GPS_PositionAltitude_data, data, dlc);
	case 0x625: return unpack_can_0x625_GPS_Delusions_A(&can_0x625_GPS_Delusions_A_data, data, dlc);
	case 0x626: return unpack_can_0x626_GPS_Delusions_B(&can_0x626_GPS_Delusions_B_data, data, dlc);
	case 0x627: return unpack_can_0x627_GPS_DateTime(&can_0x627_GPS_DateTime_data, data, dlc);
	case 0x630: return unpack_can_0x630_IO(&can_0x630_IO_data, data, dlc);
	case 0x640: return unpack_can_0x640_RTC_DateTime(&can_0x640_RTC_DateTime_data, data, dlc);
	case 0x650: return unpack_can_0x650_Out_IO(&can_0x650_Out_IO_data, data, dlc);
	case 0x651: return unpack_can_0x651_Out_PowerOff(&can_0x651_Out_PowerOff_data, data, dlc);
	case 0x652: return unpack_can_0x652_Out_Gyro(&can_0x652_Out_Gyro_data, data, dlc);
	case 0x653: return unpack_can_0x653_Out_BMC_AccScale(&can_0x653_Out_BMC_AccScale_data, data, dlc);
	case 0x654: return unpack_can_0x654_Out_SaveConfig(&can_0x654_Out_SaveConfig_data, data, dlc);
	case 0x655: return unpack_can_0x655_Out_RTC_SetTime(&can_0x655_Out_RTC_SetTime_data, data, dlc);
	case 0x656: return unpack_can_0x656_Out_RTC_TimeFromGPS(&can_0x656_Out_RTC_TimeFromGPS_data, data, dlc);
	case 0x657: return unpack_can_0x657_Out_Acc_FastCalibration(&can_0x657_Out_Acc_FastCalibration_data, data, dlc);
	default: break; 
	}
	return -1; 
}

int pack_message(unsigned id, uint64_t *data)
{
	switch(id) {
	case 0x600: return pack_can_0x600_BMC_Acceleration(&can_0x600_BMC_Acceleration_data, data);
	case 0x601: return pack_can_0x601_BMC_MagneticField(&can_0x601_BMC_MagneticField_data, data);
	case 0x610: return pack_can_0x610_L3GD20_Rotation_A(&can_0x610_L3GD20_Rotation_A_data, data);
	case 0x611: return pack_can_0x611_L3GD20_Rotation_B(&can_0x611_L3GD20_Rotation_B_data, data);
	case 0x620: return pack_can_0x620_GPS_Status(&can_0x620_GPS_Status_data, data);
	case 0x621: return pack_can_0x621_GPS_CourseSpeed(&can_0x621_GPS_CourseSpeed_data, data);
	case 0x622: return pack_can_0x622_GPS_PositionLongitude(&can_0x622_GPS_PositionLongitude_data, data);
	case 0x623: return pack_can_0x623_GPS_PositionLatitude(&can_0x623_GPS_PositionLatitude_data, data);
	case 0x624: return pack_can_0x624_GPS_PositionAltitude(&can_0x624_GPS_PositionAltitude_data, data);
	case 0x625: return pack_can_0x625_GPS_Delusions_A(&can_0x625_GPS_Delusions_A_data, data);
	case 0x626: return pack_can_0x626_GPS_Delusions_B(&can_0x626_GPS_Delusions_B_data, data);
	case 0x627: return pack_can_0x627_GPS_DateTime(&can_0x627_GPS_DateTime_data, data);
	case 0x630: return pack_can_0x630_IO(&can_0x630_IO_data, data);
	case 0x640: return pack_can_0x640_RTC_DateTime(&can_0x640_RTC_DateTime_data, data);
	case 0x650: return pack_can_0x650_Out_IO(&can_0x650_Out_IO_data, data);
	case 0x651: return pack_can_0x651_Out_PowerOff(&can_0x651_Out_PowerOff_data, data);
	case 0x652: return pack_can_0x652_Out_Gyro(&can_0x652_Out_Gyro_data, data);
	case 0x653: return pack_can_0x653_Out_BMC_AccScale(&can_0x653_Out_BMC_AccScale_data, data);
	case 0x654: return pack_can_0x654_Out_SaveConfig(&can_0x654_Out_SaveConfig_data, data);
	case 0x655: return pack_can_0x655_Out_RTC_SetTime(&can_0x655_Out_RTC_SetTime_data, data);
	case 0x656: return pack_can_0x656_Out_RTC_TimeFromGPS(&can_0x656_Out_RTC_TimeFromGPS_data, data);
	case 0x657: return pack_can_0x657_Out_Acc_FastCalibration(&can_0x657_Out_Acc_FastCalibration_data, data);
	default: break; 
	}
	return -1; 
}

int print_message(unsigned id, FILE* data)
{
	switch(id) {
	case 0x600: return print_can_0x600_BMC_Acceleration(&can_0x600_BMC_Acceleration_data, data);
	case 0x601: return print_can_0x601_BMC_MagneticField(&can_0x601_BMC_MagneticField_data, data);
	case 0x610: return print_can_0x610_L3GD20_Rotation_A(&can_0x610_L3GD20_Rotation_A_data, data);
	case 0x611: return print_can_0x611_L3GD20_Rotation_B(&can_0x611_L3GD20_Rotation_B_data, data);
	case 0x620: return print_can_0x620_GPS_Status(&can_0x620_GPS_Status_data, data);
	case 0x621: return print_can_0x621_GPS_CourseSpeed(&can_0x621_GPS_CourseSpeed_data, data);
	case 0x622: return print_can_0x622_GPS_PositionLongitude(&can_0x622_GPS_PositionLongitude_data, data);
	case 0x623: return print_can_0x623_GPS_PositionLatitude(&can_0x623_GPS_PositionLatitude_data, data);
	case 0x624: return print_can_0x624_GPS_PositionAltitude(&can_0x624_GPS_PositionAltitude_data, data);
	case 0x625: return print_can_0x625_GPS_Delusions_A(&can_0x625_GPS_Delusions_A_data, data);
	case 0x626: return print_can_0x626_GPS_Delusions_B(&can_0x626_GPS_Delusions_B_data, data);
	case 0x627: return print_can_0x627_GPS_DateTime(&can_0x627_GPS_DateTime_data, data);
	case 0x630: return print_can_0x630_IO(&can_0x630_IO_data, data);
	case 0x640: return print_can_0x640_RTC_DateTime(&can_0x640_RTC_DateTime_data, data);
	case 0x650: return print_can_0x650_Out_IO(&can_0x650_Out_IO_data, data);
	case 0x651: return print_can_0x651_Out_PowerOff(&can_0x651_Out_PowerOff_data, data);
	case 0x652: return print_can_0x652_Out_Gyro(&can_0x652_Out_Gyro_data, data);
	case 0x653: return print_can_0x653_Out_BMC_AccScale(&can_0x653_Out_BMC_AccScale_data, data);
	case 0x654: return print_can_0x654_Out_SaveConfig(&can_0x654_Out_SaveConfig_data, data);
	case 0x655: return print_can_0x655_Out_RTC_SetTime(&can_0x655_Out_RTC_SetTime_data, data);
	case 0x656: return print_can_0x656_Out_RTC_TimeFromGPS(&can_0x656_Out_RTC_TimeFromGPS_data, data);
	case 0x657: return print_can_0x657_Out_Acc_FastCalibration(&can_0x657_Out_Acc_FastCalibration_data, data);
	default: break; 
	}
	return -1; 
}

can_0x600_BMC_Acceleration_t can_0x600_BMC_Acceleration_data;

int pack_can_0x600_BMC_Acceleration(can_0x600_BMC_Acceleration_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* Acceleration_X: start-bit 0, length 16, endianess intel, scaling 3.910000, offset 0.000000 */
	x = (*(uint16_t*)(&pack->Acceleration_X)) & 0xffff;
	i |= x;
	/* Acceleration_Y: start-bit 16, length 16, endianess intel, scaling 3.910000, offset 0.000000 */
	x = (*(uint16_t*)(&pack->Acceleration_Y)) & 0xffff;
	x <<= 16; 
	i |= x;
	/* Acceleration_Z: start-bit 32, length 16, endianess intel, scaling 3.910000, offset 0.000000 */
	x = (*(uint16_t*)(&pack->Acceleration_Z)) & 0xffff;
	x <<= 32; 
	i |= x;
	/* Temperature: start-bit 48, length 8, endianess intel, scaling 0.500000, offset 24.000000 */
	x = (*(uint8_t*)(&pack->Temperature)) & 0xff;
	x <<= 48; 
	i |= x;
	/* Orientation: start-bit 58, length 3, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Orientation)) & 0x7;
	x <<= 58; 
	i |= x;
	/* VerticalAxis: start-bit 56, length 2, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->VerticalAxis)) & 0x3;
	x <<= 56; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x600_BMC_Acceleration(can_0x600_BMC_Acceleration_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 8)
		return -1;
	/* Acceleration_X: start-bit 0, length 16, endianess intel, scaling 3.910000, offset 0.000000 */
	x = i & 0xffff;
	unpack->Acceleration_X = x;
	/* Acceleration_Y: start-bit 16, length 16, endianess intel, scaling 3.910000, offset 0.000000 */
	x = (i >> 16) & 0xffff;
	unpack->Acceleration_Y = x;
	/* Acceleration_Z: start-bit 32, length 16, endianess intel, scaling 3.910000, offset 0.000000 */
	x = (i >> 32) & 0xffff;
	unpack->Acceleration_Z = x;
	/* Temperature: start-bit 48, length 8, endianess intel, scaling 0.500000, offset 24.000000 */
	x = (i >> 48) & 0xff;
	unpack->Temperature = x;
	/* Orientation: start-bit 58, length 3, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 58) & 0x7;
	unpack->Orientation = x;
	/* VerticalAxis: start-bit 56, length 2, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 56) & 0x3;
	unpack->VerticalAxis = x;
	return 0;
}

int print_can_0x600_BMC_Acceleration(can_0x600_BMC_Acceleration_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x600_Acceleration_X(print);
	r = fprintf(data, "Acceleration_X = %.3f (wire: %.0f)\n", scaled, (double)(print->Acceleration_X));
	scaled = decode_can_0x600_Acceleration_Y(print);
	r = fprintf(data, "Acceleration_Y = %.3f (wire: %.0f)\n", scaled, (double)(print->Acceleration_Y));
	scaled = decode_can_0x600_Acceleration_Z(print);
	r = fprintf(data, "Acceleration_Z = %.3f (wire: %.0f)\n", scaled, (double)(print->Acceleration_Z));
	scaled = decode_can_0x600_Temperature(print);
	r = fprintf(data, "Temperature = %.3f (wire: %.0f)\n", scaled, (double)(print->Temperature));
	scaled = decode_can_0x600_Orientation(print);
	r = fprintf(data, "Orientation = %.3f (wire: %.0f)\n", scaled, (double)(print->Orientation));
	scaled = decode_can_0x600_VerticalAxis(print);
	r = fprintf(data, "VerticalAxis = %.3f (wire: %.0f)\n", scaled, (double)(print->VerticalAxis));
	return r;
}

can_0x601_BMC_MagneticField_t can_0x601_BMC_MagneticField_data;

int pack_can_0x601_BMC_MagneticField(can_0x601_BMC_MagneticField_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* MagneticField_X: start-bit 0, length 16, endianess intel, scaling 0.300000, offset 0.000000 */
	x = (*(uint16_t*)(&pack->MagneticField_X)) & 0xffff;
	i |= x;
	/* MagneticField_Y: start-bit 16, length 16, endianess intel, scaling 0.300000, offset 0.000000 */
	x = (*(uint16_t*)(&pack->MagneticField_Y)) & 0xffff;
	x <<= 16; 
	i |= x;
	/* MagneticField_Z: start-bit 32, length 16, endianess intel, scaling 0.300000, offset 0.000000 */
	x = (*(uint16_t*)(&pack->MagneticField_Z)) & 0xffff;
	x <<= 32; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x601_BMC_MagneticField(can_0x601_BMC_MagneticField_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 6)
		return -1;
	/* MagneticField_X: start-bit 0, length 16, endianess intel, scaling 0.300000, offset 0.000000 */
	x = i & 0xffff;
	unpack->MagneticField_X = x;
	/* MagneticField_Y: start-bit 16, length 16, endianess intel, scaling 0.300000, offset 0.000000 */
	x = (i >> 16) & 0xffff;
	unpack->MagneticField_Y = x;
	/* MagneticField_Z: start-bit 32, length 16, endianess intel, scaling 0.300000, offset 0.000000 */
	x = (i >> 32) & 0xffff;
	unpack->MagneticField_Z = x;
	return 0;
}

int print_can_0x601_BMC_MagneticField(can_0x601_BMC_MagneticField_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x601_MagneticField_X(print);
	r = fprintf(data, "MagneticField_X = %.3f (wire: %.0f)\n", scaled, (double)(print->MagneticField_X));
	scaled = decode_can_0x601_MagneticField_Y(print);
	r = fprintf(data, "MagneticField_Y = %.3f (wire: %.0f)\n", scaled, (double)(print->MagneticField_Y));
	scaled = decode_can_0x601_MagneticField_Z(print);
	r = fprintf(data, "MagneticField_Z = %.3f (wire: %.0f)\n", scaled, (double)(print->MagneticField_Z));
	return r;
}

can_0x610_L3GD20_Rotation_A_t can_0x610_L3GD20_Rotation_A_data;

int pack_can_0x610_L3GD20_Rotation_A(can_0x610_L3GD20_Rotation_A_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* Rotation_X: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint32_t*)(&pack->Rotation_X)) & 0xffffffff;
	i |= x;
	/* Rotation_Y: start-bit 32, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint32_t*)(&pack->Rotation_Y)) & 0xffffffff;
	x <<= 32; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x610_L3GD20_Rotation_A(can_0x610_L3GD20_Rotation_A_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 8)
		return -1;
	/* Rotation_X: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xffffffff;
	unpack->Rotation_X = x;
	/* Rotation_Y: start-bit 32, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 32) & 0xffffffff;
	unpack->Rotation_Y = x;
	return 0;
}

int print_can_0x610_L3GD20_Rotation_A(can_0x610_L3GD20_Rotation_A_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x610_Rotation_X(print);
	r = fprintf(data, "Rotation_X = %.3f (wire: %.0f)\n", scaled, (double)(print->Rotation_X));
	scaled = decode_can_0x610_Rotation_Y(print);
	r = fprintf(data, "Rotation_Y = %.3f (wire: %.0f)\n", scaled, (double)(print->Rotation_Y));
	return r;
}

can_0x611_L3GD20_Rotation_B_t can_0x611_L3GD20_Rotation_B_data;

int pack_can_0x611_L3GD20_Rotation_B(can_0x611_L3GD20_Rotation_B_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* Rotation_Z: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint32_t*)(&pack->Rotation_Z)) & 0xffffffff;
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x611_L3GD20_Rotation_B(can_0x611_L3GD20_Rotation_B_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 4)
		return -1;
	/* Rotation_Z: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xffffffff;
	unpack->Rotation_Z = x;
	return 0;
}

int print_can_0x611_L3GD20_Rotation_B(can_0x611_L3GD20_Rotation_B_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x611_Rotation_Z(print);
	r = fprintf(data, "Rotation_Z = %.3f (wire: %.0f)\n", scaled, (double)(print->Rotation_Z));
	return r;
}

can_0x620_GPS_Status_t can_0x620_GPS_Status_data;

int pack_can_0x620_GPS_Status(can_0x620_GPS_Status_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* GPS_AntennaStatus: start-bit 0, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->GPS_AntennaStatus)) & 0xff;
	i |= x;
	/* GPS_NumSatellites: start-bit 8, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->GPS_NumSatellites)) & 0xff;
	x <<= 8; 
	i |= x;
	/* GPS_NavigationMethod: start-bit 16, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->GPS_NavigationMethod)) & 0xff;
	x <<= 16; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x620_GPS_Status(can_0x620_GPS_Status_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 3)
		return -1;
	/* GPS_AntennaStatus: start-bit 0, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xff;
	unpack->GPS_AntennaStatus = x;
	/* GPS_NumSatellites: start-bit 8, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 8) & 0xff;
	unpack->GPS_NumSatellites = x;
	/* GPS_NavigationMethod: start-bit 16, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 16) & 0xff;
	unpack->GPS_NavigationMethod = x;
	return 0;
}

int print_can_0x620_GPS_Status(can_0x620_GPS_Status_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x620_GPS_AntennaStatus(print);
	r = fprintf(data, "GPS_AntennaStatus = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_AntennaStatus));
	scaled = decode_can_0x620_GPS_NumSatellites(print);
	r = fprintf(data, "GPS_NumSatellites = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_NumSatellites));
	scaled = decode_can_0x620_GPS_NavigationMethod(print);
	r = fprintf(data, "GPS_NavigationMethod = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_NavigationMethod));
	return r;
}

can_0x621_GPS_CourseSpeed_t can_0x621_GPS_CourseSpeed_data;

int pack_can_0x621_GPS_CourseSpeed(can_0x621_GPS_CourseSpeed_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* GPS_Course: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint32_t*)(&pack->GPS_Course)) & 0xffffffff;
	i |= x;
	/* GPS_Speed: start-bit 32, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint32_t*)(&pack->GPS_Speed)) & 0xffffffff;
	x <<= 32; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x621_GPS_CourseSpeed(can_0x621_GPS_CourseSpeed_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 8)
		return -1;
	/* GPS_Course: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xffffffff;
	unpack->GPS_Course = x;
	/* GPS_Speed: start-bit 32, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 32) & 0xffffffff;
	unpack->GPS_Speed = x;
	return 0;
}

int print_can_0x621_GPS_CourseSpeed(can_0x621_GPS_CourseSpeed_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x621_GPS_Course(print);
	r = fprintf(data, "GPS_Course = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_Course));
	scaled = decode_can_0x621_GPS_Speed(print);
	r = fprintf(data, "GPS_Speed = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_Speed));
	return r;
}

can_0x622_GPS_PositionLongitude_t can_0x622_GPS_PositionLongitude_data;

int pack_can_0x622_GPS_PositionLongitude(can_0x622_GPS_PositionLongitude_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* GPS_Longitude_Minutes: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint32_t*)(&pack->GPS_Longitude_Minutes)) & 0xffffffff;
	i |= x;
	/* GPS_Longitude_Degree: start-bit 32, length 16, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint16_t*)(&pack->GPS_Longitude_Degree)) & 0xffff;
	x <<= 32; 
	i |= x;
	/* GPS_IndicatorEW: start-bit 48, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->GPS_IndicatorEW)) & 0xff;
	x <<= 48; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x622_GPS_PositionLongitude(can_0x622_GPS_PositionLongitude_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 7)
		return -1;
	/* GPS_Longitude_Minutes: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xffffffff;
	unpack->GPS_Longitude_Minutes = x;
	/* GPS_Longitude_Degree: start-bit 32, length 16, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 32) & 0xffff;
	unpack->GPS_Longitude_Degree = x;
	/* GPS_IndicatorEW: start-bit 48, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 48) & 0xff;
	unpack->GPS_IndicatorEW = x;
	return 0;
}

int print_can_0x622_GPS_PositionLongitude(can_0x622_GPS_PositionLongitude_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x622_GPS_Longitude_Minutes(print);
	r = fprintf(data, "GPS_Longitude_Minutes = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_Longitude_Minutes));
	scaled = decode_can_0x622_GPS_Longitude_Degree(print);
	r = fprintf(data, "GPS_Longitude_Degree = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_Longitude_Degree));
	scaled = decode_can_0x622_GPS_IndicatorEW(print);
	r = fprintf(data, "GPS_IndicatorEW = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_IndicatorEW));
	return r;
}

can_0x623_GPS_PositionLatitude_t can_0x623_GPS_PositionLatitude_data;

int pack_can_0x623_GPS_PositionLatitude(can_0x623_GPS_PositionLatitude_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* GPS_Latitude_Minutes: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint32_t*)(&pack->GPS_Latitude_Minutes)) & 0xffffffff;
	i |= x;
	/* GPS_Latitude_Degree: start-bit 32, length 16, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint16_t*)(&pack->GPS_Latitude_Degree)) & 0xffff;
	x <<= 32; 
	i |= x;
	/* GPS_IndicatorNS: start-bit 48, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->GPS_IndicatorNS)) & 0xff;
	x <<= 48; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x623_GPS_PositionLatitude(can_0x623_GPS_PositionLatitude_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 7)
		return -1;
	/* GPS_Latitude_Minutes: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xffffffff;
	unpack->GPS_Latitude_Minutes = x;
	/* GPS_Latitude_Degree: start-bit 32, length 16, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 32) & 0xffff;
	unpack->GPS_Latitude_Degree = x;
	/* GPS_IndicatorNS: start-bit 48, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 48) & 0xff;
	unpack->GPS_IndicatorNS = x;
	return 0;
}

int print_can_0x623_GPS_PositionLatitude(can_0x623_GPS_PositionLatitude_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x623_GPS_Latitude_Minutes(print);
	r = fprintf(data, "GPS_Latitude_Minutes = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_Latitude_Minutes));
	scaled = decode_can_0x623_GPS_Latitude_Degree(print);
	r = fprintf(data, "GPS_Latitude_Degree = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_Latitude_Degree));
	scaled = decode_can_0x623_GPS_IndicatorNS(print);
	r = fprintf(data, "GPS_IndicatorNS = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_IndicatorNS));
	return r;
}

can_0x624_GPS_PositionAltitude_t can_0x624_GPS_PositionAltitude_data;

int pack_can_0x624_GPS_PositionAltitude(can_0x624_GPS_PositionAltitude_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* GPS_Altitude: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint32_t*)(&pack->GPS_Altitude)) & 0xffffffff;
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x624_GPS_PositionAltitude(can_0x624_GPS_PositionAltitude_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 4)
		return -1;
	/* GPS_Altitude: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xffffffff;
	unpack->GPS_Altitude = x;
	return 0;
}

int print_can_0x624_GPS_PositionAltitude(can_0x624_GPS_PositionAltitude_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x624_GPS_Altitude(print);
	r = fprintf(data, "GPS_Altitude = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_Altitude));
	return r;
}

can_0x625_GPS_Delusions_A_t can_0x625_GPS_Delusions_A_data;

int pack_can_0x625_GPS_Delusions_A(can_0x625_GPS_Delusions_A_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* GPS_PDOP: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint32_t*)(&pack->GPS_PDOP)) & 0xffffffff;
	i |= x;
	/* GPS_HDOP: start-bit 32, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint32_t*)(&pack->GPS_HDOP)) & 0xffffffff;
	x <<= 32; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x625_GPS_Delusions_A(can_0x625_GPS_Delusions_A_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 8)
		return -1;
	/* GPS_PDOP: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xffffffff;
	unpack->GPS_PDOP = x;
	/* GPS_HDOP: start-bit 32, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 32) & 0xffffffff;
	unpack->GPS_HDOP = x;
	return 0;
}

int print_can_0x625_GPS_Delusions_A(can_0x625_GPS_Delusions_A_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x625_GPS_PDOP(print);
	r = fprintf(data, "GPS_PDOP = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_PDOP));
	scaled = decode_can_0x625_GPS_HDOP(print);
	r = fprintf(data, "GPS_HDOP = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_HDOP));
	return r;
}

can_0x626_GPS_Delusions_B_t can_0x626_GPS_Delusions_B_data;

int pack_can_0x626_GPS_Delusions_B(can_0x626_GPS_Delusions_B_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* GPS_VDOP: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint32_t*)(&pack->GPS_VDOP)) & 0xffffffff;
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x626_GPS_Delusions_B(can_0x626_GPS_Delusions_B_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 4)
		return -1;
	/* GPS_VDOP: start-bit 0, length 32, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xffffffff;
	unpack->GPS_VDOP = x;
	return 0;
}

int print_can_0x626_GPS_Delusions_B(can_0x626_GPS_Delusions_B_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x626_GPS_VDOP(print);
	r = fprintf(data, "GPS_VDOP = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_VDOP));
	return r;
}

can_0x627_GPS_DateTime_t can_0x627_GPS_DateTime_data;

int pack_can_0x627_GPS_DateTime(can_0x627_GPS_DateTime_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* UTC_Year: start-bit 0, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->UTC_Year)) & 0xff;
	i |= x;
	/* UTC_Month: start-bit 8, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->UTC_Month)) & 0xff;
	x <<= 8; 
	i |= x;
	/* UTC_DayOfMonth: start-bit 16, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->UTC_DayOfMonth)) & 0xff;
	x <<= 16; 
	i |= x;
	/* UTC_Hour: start-bit 24, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->UTC_Hour)) & 0xff;
	x <<= 24; 
	i |= x;
	/* UTC_Minute: start-bit 32, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->UTC_Minute)) & 0xff;
	x <<= 32; 
	i |= x;
	/* UTC_Second: start-bit 40, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->UTC_Second)) & 0xff;
	x <<= 40; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x627_GPS_DateTime(can_0x627_GPS_DateTime_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 6)
		return -1;
	/* UTC_Year: start-bit 0, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xff;
	unpack->UTC_Year = x;
	/* UTC_Month: start-bit 8, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 8) & 0xff;
	unpack->UTC_Month = x;
	/* UTC_DayOfMonth: start-bit 16, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 16) & 0xff;
	unpack->UTC_DayOfMonth = x;
	/* UTC_Hour: start-bit 24, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 24) & 0xff;
	unpack->UTC_Hour = x;
	/* UTC_Minute: start-bit 32, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 32) & 0xff;
	unpack->UTC_Minute = x;
	/* UTC_Second: start-bit 40, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 40) & 0xff;
	unpack->UTC_Second = x;
	return 0;
}

int print_can_0x627_GPS_DateTime(can_0x627_GPS_DateTime_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x627_UTC_Year(print);
	r = fprintf(data, "UTC_Year = %.3f (wire: %.0f)\n", scaled, (double)(print->UTC_Year));
	scaled = decode_can_0x627_UTC_Month(print);
	r = fprintf(data, "UTC_Month = %.3f (wire: %.0f)\n", scaled, (double)(print->UTC_Month));
	scaled = decode_can_0x627_UTC_DayOfMonth(print);
	r = fprintf(data, "UTC_DayOfMonth = %.3f (wire: %.0f)\n", scaled, (double)(print->UTC_DayOfMonth));
	scaled = decode_can_0x627_UTC_Hour(print);
	r = fprintf(data, "UTC_Hour = %.3f (wire: %.0f)\n", scaled, (double)(print->UTC_Hour));
	scaled = decode_can_0x627_UTC_Minute(print);
	r = fprintf(data, "UTC_Minute = %.3f (wire: %.0f)\n", scaled, (double)(print->UTC_Minute));
	scaled = decode_can_0x627_UTC_Second(print);
	r = fprintf(data, "UTC_Second = %.3f (wire: %.0f)\n", scaled, (double)(print->UTC_Second));
	return r;
}

can_0x630_IO_t can_0x630_IO_data;

int pack_can_0x630_IO(can_0x630_IO_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* Device_ID: start-bit 5, length 3, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Device_ID)) & 0x7;
	x <<= 5; 
	i |= x;
	/* Din1_Status: start-bit 0, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Din1_Status)) & 0x1;
	i |= x;
	/* Din2_Status: start-bit 1, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Din2_Status)) & 0x1;
	x <<= 1; 
	i |= x;
	/* Dout_Status: start-bit 2, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Dout_Status)) & 0x1;
	x <<= 2; 
	i |= x;
	/* SD_Present: start-bit 3, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->SD_Present)) & 0x1;
	x <<= 3; 
	i |= x;
	/* GPS_PowerStatus: start-bit 4, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->GPS_PowerStatus)) & 0x1;
	x <<= 4; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x630_IO(can_0x630_IO_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 1)
		return -1;
	/* Device_ID: start-bit 5, length 3, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 5) & 0x7;
	unpack->Device_ID = x;
	/* Din1_Status: start-bit 0, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0x1;
	unpack->Din1_Status = x;
	/* Din2_Status: start-bit 1, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 1) & 0x1;
	unpack->Din2_Status = x;
	/* Dout_Status: start-bit 2, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 2) & 0x1;
	unpack->Dout_Status = x;
	/* SD_Present: start-bit 3, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 3) & 0x1;
	unpack->SD_Present = x;
	/* GPS_PowerStatus: start-bit 4, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 4) & 0x1;
	unpack->GPS_PowerStatus = x;
	return 0;
}

int print_can_0x630_IO(can_0x630_IO_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x630_Device_ID(print);
	r = fprintf(data, "Device_ID = %.3f (wire: %.0f)\n", scaled, (double)(print->Device_ID));
	scaled = decode_can_0x630_Din1_Status(print);
	r = fprintf(data, "Din1_Status = %.3f (wire: %.0f)\n", scaled, (double)(print->Din1_Status));
	scaled = decode_can_0x630_Din2_Status(print);
	r = fprintf(data, "Din2_Status = %.3f (wire: %.0f)\n", scaled, (double)(print->Din2_Status));
	scaled = decode_can_0x630_Dout_Status(print);
	r = fprintf(data, "Dout_Status = %.3f (wire: %.0f)\n", scaled, (double)(print->Dout_Status));
	scaled = decode_can_0x630_SD_Present(print);
	r = fprintf(data, "SD_Present = %.3f (wire: %.0f)\n", scaled, (double)(print->SD_Present));
	scaled = decode_can_0x630_GPS_PowerStatus(print);
	r = fprintf(data, "GPS_PowerStatus = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_PowerStatus));
	return r;
}

can_0x640_RTC_DateTime_t can_0x640_RTC_DateTime_data;

int pack_can_0x640_RTC_DateTime(can_0x640_RTC_DateTime_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* RTC_Year: start-bit 48, length 16, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint16_t*)(&pack->RTC_Year)) & 0xffff;
	x <<= 48; 
	i |= x;
	/* RTC_Sec: start-bit 0, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_Sec)) & 0xff;
	i |= x;
	/* RTC_Min: start-bit 8, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_Min)) & 0xff;
	x <<= 8; 
	i |= x;
	/* RTC_Hour: start-bit 16, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_Hour)) & 0xff;
	x <<= 16; 
	i |= x;
	/* RTC_DayOfWeek: start-bit 24, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_DayOfWeek)) & 0xff;
	x <<= 24; 
	i |= x;
	/* RTC_DayOfMonth: start-bit 32, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_DayOfMonth)) & 0xff;
	x <<= 32; 
	i |= x;
	/* RTC_Month: start-bit 40, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_Month)) & 0xff;
	x <<= 40; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x640_RTC_DateTime(can_0x640_RTC_DateTime_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 8)
		return -1;
	/* RTC_Year: start-bit 48, length 16, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 48) & 0xffff;
	unpack->RTC_Year = x;
	/* RTC_Sec: start-bit 0, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xff;
	unpack->RTC_Sec = x;
	/* RTC_Min: start-bit 8, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 8) & 0xff;
	unpack->RTC_Min = x;
	/* RTC_Hour: start-bit 16, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 16) & 0xff;
	unpack->RTC_Hour = x;
	/* RTC_DayOfWeek: start-bit 24, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 24) & 0xff;
	unpack->RTC_DayOfWeek = x;
	/* RTC_DayOfMonth: start-bit 32, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 32) & 0xff;
	unpack->RTC_DayOfMonth = x;
	/* RTC_Month: start-bit 40, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 40) & 0xff;
	unpack->RTC_Month = x;
	return 0;
}

int print_can_0x640_RTC_DateTime(can_0x640_RTC_DateTime_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x640_RTC_Year(print);
	r = fprintf(data, "RTC_Year = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_Year));
	scaled = decode_can_0x640_RTC_Sec(print);
	r = fprintf(data, "RTC_Sec = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_Sec));
	scaled = decode_can_0x640_RTC_Min(print);
	r = fprintf(data, "RTC_Min = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_Min));
	scaled = decode_can_0x640_RTC_Hour(print);
	r = fprintf(data, "RTC_Hour = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_Hour));
	scaled = decode_can_0x640_RTC_DayOfWeek(print);
	r = fprintf(data, "RTC_DayOfWeek = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_DayOfWeek));
	scaled = decode_can_0x640_RTC_DayOfMonth(print);
	r = fprintf(data, "RTC_DayOfMonth = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_DayOfMonth));
	scaled = decode_can_0x640_RTC_Month(print);
	r = fprintf(data, "RTC_Month = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_Month));
	return r;
}

can_0x650_Out_IO_t can_0x650_Out_IO_data;

int pack_can_0x650_Out_IO(can_0x650_Out_IO_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* Dout_Set: start-bit 0, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Dout_Set)) & 0x1;
	i |= x;
	/* GPS_SetPower: start-bit 1, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->GPS_SetPower)) & 0x1;
	x <<= 1; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x650_Out_IO(can_0x650_Out_IO_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 1)
		return -1;
	/* Dout_Set: start-bit 0, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0x1;
	unpack->Dout_Set = x;
	/* GPS_SetPower: start-bit 1, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 1) & 0x1;
	unpack->GPS_SetPower = x;
	return 0;
}

int print_can_0x650_Out_IO(can_0x650_Out_IO_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x650_Dout_Set(print);
	r = fprintf(data, "Dout_Set = %.3f (wire: %.0f)\n", scaled, (double)(print->Dout_Set));
	scaled = decode_can_0x650_GPS_SetPower(print);
	r = fprintf(data, "GPS_SetPower = %.3f (wire: %.0f)\n", scaled, (double)(print->GPS_SetPower));
	return r;
}

can_0x651_Out_PowerOff_t can_0x651_Out_PowerOff_data;

int pack_can_0x651_Out_PowerOff(can_0x651_Out_PowerOff_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* Device_PowerOff: start-bit 0, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Device_PowerOff)) & 0x1;
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x651_Out_PowerOff(can_0x651_Out_PowerOff_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 1)
		return -1;
	/* Device_PowerOff: start-bit 0, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0x1;
	unpack->Device_PowerOff = x;
	return 0;
}

int print_can_0x651_Out_PowerOff(can_0x651_Out_PowerOff_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x651_Device_PowerOff(print);
	r = fprintf(data, "Device_PowerOff = %.3f (wire: %.0f)\n", scaled, (double)(print->Device_PowerOff));
	return r;
}

can_0x652_Out_Gyro_t can_0x652_Out_Gyro_data;

int pack_can_0x652_Out_Gyro(can_0x652_Out_Gyro_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* Gyro_SetScale: start-bit 0, length 2, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Gyro_SetScale)) & 0x3;
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x652_Out_Gyro(can_0x652_Out_Gyro_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 1)
		return -1;
	/* Gyro_SetScale: start-bit 0, length 2, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0x3;
	unpack->Gyro_SetScale = x;
	return 0;
}

int print_can_0x652_Out_Gyro(can_0x652_Out_Gyro_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x652_Gyro_SetScale(print);
	r = fprintf(data, "Gyro_SetScale = %.3f (wire: %.0f)\n", scaled, (double)(print->Gyro_SetScale));
	return r;
}

can_0x653_Out_BMC_AccScale_t can_0x653_Out_BMC_AccScale_data;

int pack_can_0x653_Out_BMC_AccScale(can_0x653_Out_BMC_AccScale_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* Acc_SetScale: start-bit 0, length 3, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Acc_SetScale)) & 0x7;
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x653_Out_BMC_AccScale(can_0x653_Out_BMC_AccScale_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 1)
		return -1;
	/* Acc_SetScale: start-bit 0, length 3, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0x7;
	unpack->Acc_SetScale = x;
	return 0;
}

int print_can_0x653_Out_BMC_AccScale(can_0x653_Out_BMC_AccScale_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x653_Acc_SetScale(print);
	r = fprintf(data, "Acc_SetScale = %.3f (wire: %.0f)\n", scaled, (double)(print->Acc_SetScale));
	return r;
}

can_0x654_Out_SaveConfig_t can_0x654_Out_SaveConfig_data;

int pack_can_0x654_Out_SaveConfig(can_0x654_Out_SaveConfig_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* Config_SaveToEEPROM: start-bit 0, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Config_SaveToEEPROM)) & 0x1;
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x654_Out_SaveConfig(can_0x654_Out_SaveConfig_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 1)
		return -1;
	/* Config_SaveToEEPROM: start-bit 0, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0x1;
	unpack->Config_SaveToEEPROM = x;
	return 0;
}

int print_can_0x654_Out_SaveConfig(can_0x654_Out_SaveConfig_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x654_Config_SaveToEEPROM(print);
	r = fprintf(data, "Config_SaveToEEPROM = %.3f (wire: %.0f)\n", scaled, (double)(print->Config_SaveToEEPROM));
	return r;
}

can_0x655_Out_RTC_SetTime_t can_0x655_Out_RTC_SetTime_data;

int pack_can_0x655_Out_RTC_SetTime(can_0x655_Out_RTC_SetTime_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* RTC_SetYear: start-bit 48, length 16, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint16_t*)(&pack->RTC_SetYear)) & 0xffff;
	x <<= 48; 
	i |= x;
	/* RTC_SetSec: start-bit 0, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_SetSec)) & 0xff;
	i |= x;
	/* RTC_SetMin: start-bit 8, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_SetMin)) & 0xff;
	x <<= 8; 
	i |= x;
	/* RTC_SetHour: start-bit 16, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_SetHour)) & 0xff;
	x <<= 16; 
	i |= x;
	/* RTC_SetDayOfWeek: start-bit 24, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_SetDayOfWeek)) & 0xff;
	x <<= 24; 
	i |= x;
	/* RTC_SetDayOfMonth: start-bit 32, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_SetDayOfMonth)) & 0xff;
	x <<= 32; 
	i |= x;
	/* RTC_SetMonth: start-bit 40, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_SetMonth)) & 0xff;
	x <<= 40; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x655_Out_RTC_SetTime(can_0x655_Out_RTC_SetTime_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 8)
		return -1;
	/* RTC_SetYear: start-bit 48, length 16, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 48) & 0xffff;
	unpack->RTC_SetYear = x;
	/* RTC_SetSec: start-bit 0, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0xff;
	unpack->RTC_SetSec = x;
	/* RTC_SetMin: start-bit 8, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 8) & 0xff;
	unpack->RTC_SetMin = x;
	/* RTC_SetHour: start-bit 16, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 16) & 0xff;
	unpack->RTC_SetHour = x;
	/* RTC_SetDayOfWeek: start-bit 24, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 24) & 0xff;
	unpack->RTC_SetDayOfWeek = x;
	/* RTC_SetDayOfMonth: start-bit 32, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 32) & 0xff;
	unpack->RTC_SetDayOfMonth = x;
	/* RTC_SetMonth: start-bit 40, length 8, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 40) & 0xff;
	unpack->RTC_SetMonth = x;
	return 0;
}

int print_can_0x655_Out_RTC_SetTime(can_0x655_Out_RTC_SetTime_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x655_RTC_SetYear(print);
	r = fprintf(data, "RTC_SetYear = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_SetYear));
	scaled = decode_can_0x655_RTC_SetSec(print);
	r = fprintf(data, "RTC_SetSec = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_SetSec));
	scaled = decode_can_0x655_RTC_SetMin(print);
	r = fprintf(data, "RTC_SetMin = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_SetMin));
	scaled = decode_can_0x655_RTC_SetHour(print);
	r = fprintf(data, "RTC_SetHour = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_SetHour));
	scaled = decode_can_0x655_RTC_SetDayOfWeek(print);
	r = fprintf(data, "RTC_SetDayOfWeek = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_SetDayOfWeek));
	scaled = decode_can_0x655_RTC_SetDayOfMonth(print);
	r = fprintf(data, "RTC_SetDayOfMonth = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_SetDayOfMonth));
	scaled = decode_can_0x655_RTC_SetMonth(print);
	r = fprintf(data, "RTC_SetMonth = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_SetMonth));
	return r;
}

can_0x656_Out_RTC_TimeFromGPS_t can_0x656_Out_RTC_TimeFromGPS_data;

int pack_can_0x656_Out_RTC_TimeFromGPS(can_0x656_Out_RTC_TimeFromGPS_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* RTC_SetTimeFromGPS: start-bit 0, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->RTC_SetTimeFromGPS)) & 0x1;
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x656_Out_RTC_TimeFromGPS(can_0x656_Out_RTC_TimeFromGPS_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 1)
		return -1;
	/* RTC_SetTimeFromGPS: start-bit 0, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0x1;
	unpack->RTC_SetTimeFromGPS = x;
	return 0;
}

int print_can_0x656_Out_RTC_TimeFromGPS(can_0x656_Out_RTC_TimeFromGPS_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x656_RTC_SetTimeFromGPS(print);
	r = fprintf(data, "RTC_SetTimeFromGPS = %.3f (wire: %.0f)\n", scaled, (double)(print->RTC_SetTimeFromGPS));
	return r;
}

can_0x657_Out_Acc_FastCalibration_t can_0x657_Out_Acc_FastCalibration_data;

int pack_can_0x657_Out_Acc_FastCalibration(can_0x657_Out_Acc_FastCalibration_t *pack, uint64_t *data)
{
	register uint64_t x;
	register uint64_t i = 0;
	/* Acc_SetCalibTarget_X: start-bit 0, length 2, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Acc_SetCalibTarget_X)) & 0x3;
	i |= x;
	/* Acc_SetCalibTarget_Y: start-bit 8, length 2, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Acc_SetCalibTarget_Y)) & 0x3;
	x <<= 8; 
	i |= x;
	/* Acc_SetCalibTarget_Z: start-bit 16, length 2, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Acc_SetCalibTarget_Z)) & 0x3;
	x <<= 16; 
	i |= x;
	/* Acc_StartFastCalib: start-bit 24, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (*(uint8_t*)(&pack->Acc_StartFastCalib)) & 0x1;
	x <<= 24; 
	i |= x;
	*data = (i);
	return 0;
}

int unpack_can_0x657_Out_Acc_FastCalibration(can_0x657_Out_Acc_FastCalibration_t *unpack, uint64_t data, uint8_t dlc)
{
	register uint64_t x;
	register uint64_t i = (data);
	if(dlc < 4)
		return -1;
	/* Acc_SetCalibTarget_X: start-bit 0, length 2, endianess intel, scaling 1.000000, offset 0.000000 */
	x = i & 0x3;
	unpack->Acc_SetCalibTarget_X = x;
	/* Acc_SetCalibTarget_Y: start-bit 8, length 2, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 8) & 0x3;
	unpack->Acc_SetCalibTarget_Y = x;
	/* Acc_SetCalibTarget_Z: start-bit 16, length 2, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 16) & 0x3;
	unpack->Acc_SetCalibTarget_Z = x;
	/* Acc_StartFastCalib: start-bit 24, length 1, endianess intel, scaling 1.000000, offset 0.000000 */
	x = (i >> 24) & 0x1;
	unpack->Acc_StartFastCalib = x;
	return 0;
}

int print_can_0x657_Out_Acc_FastCalibration(can_0x657_Out_Acc_FastCalibration_t *print, FILE *data)
{
	double scaled;
	int r = 0;
	scaled = decode_can_0x657_Acc_SetCalibTarget_X(print);
	r = fprintf(data, "Acc_SetCalibTarget_X = %.3f (wire: %.0f)\n", scaled, (double)(print->Acc_SetCalibTarget_X));
	scaled = decode_can_0x657_Acc_SetCalibTarget_Y(print);
	r = fprintf(data, "Acc_SetCalibTarget_Y = %.3f (wire: %.0f)\n", scaled, (double)(print->Acc_SetCalibTarget_Y));
	scaled = decode_can_0x657_Acc_SetCalibTarget_Z(print);
	r = fprintf(data, "Acc_SetCalibTarget_Z = %.3f (wire: %.0f)\n", scaled, (double)(print->Acc_SetCalibTarget_Z));
	scaled = decode_can_0x657_Acc_StartFastCalib(print);
	r = fprintf(data, "Acc_StartFastCalib = %.3f (wire: %.0f)\n", scaled, (double)(print->Acc_StartFastCalib));
	return r;
}

