# IMU Message READER

This repo extracts a bunch of msg out of a CAN bus (dbc file) and propagates them to other ROS nodes with a publisher-subscriber paradigm. 

The demo will use 3 terminals (one for roscore, one for sender, one for receiver of messages).

### Requirements

Install [ROS](http://wiki.ros.org/it/ROS/Tutorials) and get familiar with it. Also install the relative environment variables in the bashrc.

Download the submodule(s) after you clone (`git submodule init`) or clone recursively this repo with `git clone --recurse-submodules <this_repo>`.

### Python

Requires Python2 and Python3 installed. To be launched with Python2. 

```
roscore (t1)
cd python_example
python subscriber.py (t2)
python publisher.py (t3)
```

### C++

##### Pre-build (preparing libs)

CAN msg compiler tool

```
cd cpp_example/dbcc
mkdir build && cd build
cmake .. && make
```

Lib for CAN msg schema (c-code and xml)

```
cd ../../pcan
mkdir src xml
../dbcc/build/dbcc -o src Customer_Control_Diagnostics.dbc
../dbcc/build/dbcc -x -o xml Customer_Control_Diagnostics.dbc
```

##### Build

```
cd cpp_example
catkin_make
```

##### Launch

```
roscore (t1)
cd cpp_example
./devel/lib/cpp_example/subscriber (t2)
./devel/lib/cpp_example/publisher (t3)
```