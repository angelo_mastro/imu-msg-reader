# to be activated with python3 (cantools)

import cantools
from sys import argv, exit, stdout
from time import sleep

class Reader:

    def __init__(self, filepath):
        self.filepath = filepath
        self.db = cantools.database.load_file(self.filepath)

    def __next__(self):
        yield self.db.messages


if __name__ == "__main__":
    if len(argv) < 2:
        print("missing argument <dbc_file_path>")
        exit(1)

    r = Reader(argv[1])
    todo = 1
    while todo > 0: # todo: remove infinite loop after testing and add exit(0)
        for m in next(r):
            print(m)
            stdout.flush()
        sleep(1)
        todo -= 1
    print("finished reading")
    exit(0)
