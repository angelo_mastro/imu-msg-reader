# to be interpreted by python2 (ROSPY)

import rospy
from std_msgs.msg import String
import subprocess

def canreaderpub():
    pub = rospy.Publisher("canreader", String, queue_size=10)
    rospy.init_node("canreaderpub", anonymous=True)
    rate = rospy.Rate(10) # in Hz

    proc = subprocess.Popen(['/usr/bin/python3',
                             'canreader.py', 
                             'Customer_Control_Diagnostics.dbc'], 
                            stdout=subprocess.PIPE)

    while not rospy.is_shutdown():
        output = proc.stdout.readline()
        if output == '' and proc.poll() is not None:
            print "subprocess terminated"
            break
        if output:
            str_msg = output.strip()
            print str_msg
            rospy.loginfo(str_msg)
            pub.publish(str_msg)
        rate.sleep()


if __name__ == "__main__":
    try:
        canreaderpub()
    except rospy.ROSInterruptException:
        pass
